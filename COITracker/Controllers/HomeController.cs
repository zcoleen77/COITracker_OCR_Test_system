﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using COITracker.Google;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using COITracker.DTO;

namespace COITracker.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index(string annotate = "")
        {
            ViewBag.Annotate = annotate;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            ViewBag.Annotate = "";
            return View();
        }

        [HttpPost]
        public ActionResult UploadPicture(HttpPostedFileBase file, string API_Setting)
        {
            var path = "";
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the fielname
                var fileName = Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                path = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                file.SaveAs(path);
                //File.WriteAllBytes(path, fileResult.FilecContents);
            }
            // redirect back to the index action to show the form once again
            return RedirectToAction("DetectPicture", "Home", new { filename = path, setting = API_Setting });
            // return Content("successful");
        }

        public async Task<ActionResult> DetectPicture(string filename, string setting)
        {
            ViewBag.result = "";
            if (setting == "Azure")
            {
                ViewBag.result = await DetectPictureAzure(filename);
            }
            else
            {
                ViewBag.result = await DetectPictureGoogle(filename);
            }

            return ViewBag.result;

        }

        public async Task<ActionResult> DetectPictureGoogle(string filename)
        {

            Annotate annotate = new Annotate();
            //Application.DoEvents();
            await annotate.GetText(filename, "en", "TEXT_DETECTION");
            if (string.IsNullOrEmpty(annotate.Error) == false)
                ViewBag.Annotate = "ERROR";
            else

                ViewBag.Annotate = annotate.TextResult;

            return RedirectToAction("Index", "Home", new { annotate = annotate.TextResult });
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public async Task<ActionResult> DetectPictureAzure(string filename)
        {
            var result = "";
	    string resultWords = string.Empty;
            try
            {

                HttpClient client = new HttpClient();

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", "6bc4c48cd6144c3bbcda1572d8e2dec1");

                // Request parameters.
                string requestParameters = "language=unk&detectOrientation=true";

                // Assemble the URI for the REST API Call.
                string uri = "https://westus.api.cognitive.microsoft.com/vision/v1.0/ocr" + "?" + requestParameters;

                HttpResponseMessage response;

                // Request body. Posts a locally stored JPEG image.
                byte[] byteData = GetImageAsByteArray(filename);

                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses content type "application/octet-stream".
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    // Make the REST API call.
                    response = await client.PostAsync(uri, content);
                }


                // Get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Display the JSON response.
                //Console.WriteLine("\nResponse:\n");
                result = JsonPrettyPrint(contentString);

                var dto = JsonConvert.DeserializeObject<VisionDTO>(result);

               
                dto.regions.ForEach(a => 
                {
                    a.lines.ForEach(b =>
                    {
                        b.words.ForEach(c =>
                        {
                            resultWords += c.text;
                        });
                        resultWords += Environment.NewLine;
                    });
                });
            }
            catch (Exception e)
            {
                Console.WriteLine("\n" + e.Message);
            }
            ViewBag.Annotate = resultWords;

            return RedirectToAction("Index", "Home", new { annotate = resultWords });
        }

        private static byte[] ConvertStreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


        public static byte[] GetImageAsByteArray(string imageFilePath)
        {
            using (FileStream fileStream =
                           new FileStream(imageFilePath, FileMode.Open, FileAccess.Read))
            {
                BinaryReader binaryReader = new BinaryReader(fileStream);
                return binaryReader.ReadBytes((int)fileStream.Length);
            }
        }

        static string JsonPrettyPrint(string json)
        {
            if (string.IsNullOrEmpty(json))
                return string.Empty;

            json = json.Replace(Environment.NewLine, "").Replace("\t", "");

            string INDENT_STRING = "    ";
            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();
            for (var i = 0; i < json.Length; i++)
            {
                var ch = json[i];
                switch (ch)
                {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, ++indent).ForEach(
                                item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case '}':
                    case ']':
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, --indent).ForEach(
                                item => sb.Append(INDENT_STRING));
                        }
                        sb.Append(ch);
                        break;
                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && json[--index] == '\\')
                            escaped = !escaped;
                        if (!escaped)
                            quoted = !quoted;
                        break;
                    case ',':
                        sb.Append(ch);
                        if (!quoted)
                        {
                            sb.AppendLine();
                            Enumerable.Range(0, indent).ForEach(
                                item => sb.Append(INDENT_STRING));
                        }
                        break;
                    case ':':
                        sb.Append(ch);
                        if (!quoted)
                            sb.Append(" ");
                        break;
                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }
    }

    static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> ie, Action<T> action)
        {
            foreach (var i in ie)
            {
                action(i);
            }
        }
    }
}
