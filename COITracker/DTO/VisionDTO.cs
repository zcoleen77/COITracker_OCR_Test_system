﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COITracker.DTO
{
    public class VisionDTO
    {
        public string language { get; set; }
        public string orientation { get; set; }
        public string textAngle { get; set; }
        public List<VisionRegionDTO> regions { get; set; }
    }

    public class VisionRegionDTO
    {
        public string boundingBox { get; set; }
        public List<VisionLineDTO> lines { get; set; }
    }

    public class VisionLineDTO
    {
        public string boundingBox { get; set; }
        public List<VisionWordsDTO> words { get; set; }
    }

    public class VisionWordsDTO
    {
        public string boundingBox { get; set; }
        public string text { get; set; }
    }
}